#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>
#include <pthread.h>

int main(void)
{
	SDL_Window *w;
	SDL_Renderer *r;
	SDL_Event e;
	
	int michel;

	michel = 1;
	SDL_Init(SDL_INIT_VIDEO);
	w = SDL_CreateWindow("build", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 800, 600, SDL_WINDOW_SHOWN);
	r = SDL_CreateRenderer(w, -1, SDL_RENDERER_SOFTWARE);
	while (michel)
	{
	while (SDL_PollEvent(&e))
	{
		if (e.key.keysym.sym == SDLK_ESCAPE)
			michel = 0;
	}
	}
	SDL_DestroyRenderer(r);
	SDL_DestroyWindow(w);
	SDL_Quit();
	return 0;
}
